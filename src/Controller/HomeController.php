<?php

namespace App\Controller;

use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;


/**
 * Controller de l'accueil du site
 */
class HomeController extends AbstractController
{

    /**
     * Accueil de notre site
     *
     * @Route("/", name="home")
     * @return Response
     */
    public function index(PropertyRepository $propertyRepository): Response
    {
        $lastProperties = $propertyRepository->findBy(
            ["sold"         => 0],
            ["created_at"   => 'DESC'],
            5
        );

        return $this->render('pages/home.html.twig', [
            "properties" => $lastProperties
        ]);
    }
}
